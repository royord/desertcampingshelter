# DesertCampingShelter

## Overview

Below is the knowledge and materials needed to create a shelter that can be errected without damage to the existing structures at Owl Canyon Camp Ground. The idea is to create a wind barrier structure that is a piece of plastic sandwiched between two paracord rope lines to keep the plastic from shuttering too much in the wind or otherwise tearing apart the structure.

__Please DO NOT affix anything permantely to the picnic table structure or cause damage or drill holes with this install, as priority #1 should be to do no harm to the resources provided to us as campers by the Beauro of Land Management__

https://www.blm.gov/visit/rainbow-basinowl-canyon-campground-0

![Desert Picnic Shelter](Images/DesertPicnicShelter.jpg)

A variant to this shelter was first created in the mid 1990's by a friend who wanted to ensure that the cold/winds that are frequent in February wouldn't stop them from having a great time in the desert.

_The structures that this shelter is designed for may exist elsewhere however this is the only location that I have seen them and have created this shelter._

## Materials Needed

Ordered:
- (1)   1000 ft. paracord (https://a.co/d/aLcCYtg)
- (2)   Magnets 20 total magnets (https://a.co/d/bG00Dqn)

![Desert Picnic Shelter](Images/AmazonMagnets.jpg)

Local:
- (2)   2x4 8ft long
- (1)   Box of screws (8x1 5/8" deck screws stainless) - I prefer stainless Steel with a bit of a neck to not cut the paracord
- (4)   Ratchet straps
- (1)   Roll Plastic (4mil x 10' x 33' long) - Usually this comes in 100' rolls and could make 3 shelters
- (1)   Pair of Leather Gloves

Paracord Winder (Not required but certainly nice):
- (1)   18" All Thread
- (4)   Nuts (that fit the all thread)
- (4)   Large diameter Washers
- (1)   1/2 x 6" PVC (Holds the long end of the all thread, described further below)
- (1)   Socket (Fits the nuts above)
- (1)   Drill to Socket Adapter
- (1)   Box end wrench(s) (for Nuts above)
- (1)   Cordless Drill Drill (Automated winding)
- (2)   Scraps of Plywood for better contact with paracord spool

# Preparation

## Prepping the 2x4's
1. Measure down the board every 7" inches and place a marke on the ~2" side of the board.
2. With a drill install screws along center line at the marked locations. Make sure screws are flush for trasport, these will be extened just prior to installation in the "Deployment" phase below.
3. Additionally place an extra screw at 4" from each end of the 2x4, this may come in handy when getting the paracord right at the top of the shelter.

## Measuring out the plastic
1. Measure out 35' of plastic from the roll (if you plan to cover 3 sides)
2. Carefully cut with a razor knife and pack for transport with the rest of your shelter.

## Prepping the magnets
1. Upon receipt of the magnets separate them and place a short string, or ribon, arond each one. Make sure that the string is relatively thing as thicker string will lower their holding power.
2. Done
 
# Use

## Deployment

_*Installing with at least 2 people is the most efficient especially when tightening the plastic into the structure_

1. Use your cordless drill or other driver to back the screws in the 2x4's out ~1/4"
2. Placing the 2x4 boards against the upright poles with the screws pointed away from the desired opening edge, on top of the footer and into the top of the structure.
3. Ratch strap the 2x4's to the pole and tighten so that boards aren't easily moved.
4. Tie a secure boline knot onto the end of the paracord and place this onto the lowest screw on the 2x4 and begin walking around the sides of the shelter, leaving at least 1 side open.
5. When you get to the other 2x4 at the other side, pull that run tight (HERE'S WHERE THE LEATHER GLOVES HELP), go around the screw 1/4 turn and then to the next screw and then back around the shelter the other way, repeating until you are at the top of the shelter.
6. Once at the top of the shelter, loop back exactly on top of your last run and tie everything off, make sure that this is very tight to ensure installation of the plastic goes as smoothly as possible.
7. Starting on one side go around the shelter with the plastic. If you want to open it up as you do this that would be advantageouse but don't try to put it up yet.
8. Starting on the windy side work the plastic under the two paracord lines that are ontop of one another, and then proceed to the remaining sides.
9. Reverse the process that installed the first set of cord wraps now, ensuring that the cord over the plastic covers the cord under the plastic, in this way the plastic will be better held in place and not shutter as much.
10. Repeat the reverse winding all the way down to the base of the shelter, tightening as much as possible.
11. Tie everything off at the base.
12. Place the magnets along the top edge to hold the plactic in place.
13. Place rocks found around the camp ground around the base base of the plastic to hold the bottom in place.
14. Enjoy the shelter while your camping

## Teardown

1. Reverse the steps above, winding the cord as you go so that you don't get tangles, if working with someone they can release each side of the cord as you're ready to wind it. __Consider making the winder below as it will shave hours and lots of tangles__
2. Make sure that everything is packed together for the next time that you want to use it.

# Additional

## Making the paracord winder

1. Drill a hole through 4"x4" plywood, in the center, large enough to fit the "Allthread" through.
2. Thread a nut at the end of the allthread about an inch.
3. Place a washer onto the all thread
4. Place the plywood onto the all thread
5. Place another washer onto the all thread
6. Finally thread another nut onto the all thread, near the very end.
7. Tighten nut installed in step 2 towards the nut that is on the end, step 6, sandwiching the plywood in the middle. This forms one side of the cord winder.
8. Slied spool onto the winder
8. For the other side of the winder repeate the steps above omitting the nut on the inside of the spool
9. Cover the long portion remaining out of the spool with the PVC
10. Place socket on adapter into drill at put this on the shorter side of the winder and begin winding.

## Additional Shelter Locations

Sawtooth Canyon Campgrand - a favorite for rock climbers during the winter.
https://www.blm.gov/visit/sawtooth-canyon-campground